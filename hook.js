require('shelljs/global')
const http = require('http');
const PORT = 3040;
console.info("start hook of forever")


const deploy = () => exec(
    'git pull',
    {cwd: ""},
    (status, output) => {
        console.log(status, output)
        console.info("git pull command done")
    }
)

const server = http.createServer(
    (req, response) => {
        let body = ""
        req.on(
            'data', (data) =>
                body += data
        )
        req.on(
            'end', function () {
                let data = JSON.parse(body);
                if (data.ref == "refs/heads/master")
                    deploy()
                console.log("ref", data.ref)
                console.log("repository", data.repository.name)
                if (data.commits)
                    data.commits.forEach(
                        c => {
                            console.info("messgage", c.message)
                            console.log("author", c.author.email)
                        }
                    )
            }
        )
    }
)

server.listen(
    PORT, () => {
        //Callback triggered when server is successfully listening. Hurray!
        console.log("GitHook Server listening on: http://localhost:%s", PORT);
    }
);



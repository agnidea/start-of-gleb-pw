const forever = require('forever-monitor')
const fs = require('fs')
const path = require('path')

fs.mkdir("logs",function(e){
    if(!e || (e && e.code === 'EEXIST')){
        //do something with contents
    } else {
        //debug
        console.log(e);
    }
});



const hook = new (forever.Monitor)(
    'hook.js', {
        uid: "githook",
        max: 5,
        //killTree: true,
        //sourceDir: 'root',
        watch: false,
        'logFile': './logs/hook-log-txt',
        'outFile': './logs/hook-log-out.txt',
        'errFile': './logs/hook-log-err.txt'
    }
)

console.log("checkProcess", forever.checkProcess("githook"))
console.log("checkProcess", forever.checkProcess(hook))
console.log("checkProcess", forever.checkProcess(hook.uid))
hook.start()



const server = new (forever.Monitor)(
    '../start-of-gleb-pw.js', {
        uid: "start-of-gleb-pw",
        max: 2,
        killTree: true,
        sourceDir: 'root',
        watch: true,
        'logFile': './logs/server-txt',
        'outFile': './logs/server-out.txt',
        'errFile': './logs/server-err.txt'
    }
)

server.start()
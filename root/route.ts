import {IncomingMessage} from "http";
import {ServerResponse} from "http";
import pages from "root/pages";
import {page404} from "root/pages/page404";
let blog1 = pages.staticPage("blog-001");
const routeMap = {
    "/": pages.staticPage("404"),
    "/404": pages.staticPage("404"),
    "/sense-in-the-shell": pages.staticPage("blog-001"),
    "/googlee7df1f1f2a56318e.html": "google-site-verification: googlee7df1f1f2a56318e.html"
}

const LJPosts = {
    "/3018.html": true
}

export function route(req: IncomingMessage, res: ServerResponse) {
    console.log(req.url, req.headers["x-real-ip"], req.headers["from"])

    let page = routeMap[req.url]
    if (page) {
        res.write(page)
        res.end()
    } else if (LJPosts[req.url]) {
        res.writeHead(301, {"Location": "http://glebyp.livejournal.com/" + req.url})
        res.end();
    } else {

        if (req.headers["host"] != "gleb.pw" && req.headers["from"] != "googlebot(at)googlebot.com") {
            console.log("bad boy", req.headers["host"])
            res.writeHead(302, {'Location': req.headers["x-real-ip"]})
            res.end()
        } else if (req.headers["from"] == "googlebot(at)googlebot.com") {
            res.writeHead(301, {
                "Location": "https://gleb.pw/",
                "x-robots-tag": "nofollow,nocache,noindex"
            })
            //console.log("bye bye google bot")
            res.end()
        } else {
            res.write(routeMap["/404"])
            res.end()
        }
    }

}

let enableDevMode = (req, res) => {
    let sstatic = require('node-static');
    let path = require('path');
    let file = new sstatic.Server(path.resolve('../dream'));
    req.addListener("end", () => {
        file.serve(req, res)
    }).resume()
}
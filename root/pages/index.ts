/**
 * Created by dev@gleb.pw on 19.01.2017.
 */

const path = require('path');
const appDir = path.dirname(require.main.filename);
const marko = require('marko')


const loadTemplate = (pathMarko) => marko.load(require.resolve(path.join(appDir, './components', pathMarko)))
const template = loadTemplate("index/template.marko")


let temp = template.renderToString({
    name: 'Frank',
    colors: ['red', 'green', 'blue']
});

// console.log(temp)

export default class page {

    static staticPage(name):string {
        return loadTemplate(name+"/template.marko").renderToString()
    }
}